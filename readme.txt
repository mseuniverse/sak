sak : Speecher Assistive Kit. 

sak uses the PortAudio and eSpeak Open Source Libraries.

/sak => for all types of licenses, included commercial applications without source.  It uses eSpeak executable and Portaudio library.
/sak_dll => only for applications with GPL Open Source licences.  It uses eSpeak dynamic library and new Portaudio dynamic library.

/sak_mse => for msegui applications.

Included in the package:
. Binary libraries for Linux 32/64, Windows, Mac and freeBSD.
. espeak-data (multi language).

There are compiled demos for Linux 32/64, Windows, Mac and FreeBSD
 =>https://github.com/fredvs/sak/releases/ 

Fred van Stappen
fiens@hotmail.com

